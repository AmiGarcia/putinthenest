﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PutInTheNest_Trampoline : MonoBehaviour {

	public float speedUp;
	private RectTransform trampolineRect;
	private Vector2 contactPoint;
	// Use this for initialization
	void Start () {
		speedUp = 3.0f;
		trampolineRect = gameObject.GetComponent<RectTransform>();
	}

	void OnCollisionEnter2D(Collision2D coll) {
		if (coll.gameObject.tag == "ObjectFalling") {
			if(coll.otherCollider.tag == "TrampolineLeft"){
				coll.gameObject.GetComponent<Rigidbody2D> ().AddForce (new Vector2(-0.5f,1.0f) * 200);
				return;
			}
			if(coll.otherCollider.tag == "TrampolineMiddle"){
				coll.gameObject.GetComponent<Rigidbody2D> ().AddForce (new Vector2(0.02f,1.0f) * 200);
				return;
			}
			if(coll.otherCollider.tag == "TrampolineRight"){
				coll.gameObject.GetComponent<Rigidbody2D> ().AddForce (new Vector2(0.5f,1.0f) * 200);
				return;
			}

		}

	}
}
