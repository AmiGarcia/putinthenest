﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PutInTheNest_Floor : MonoBehaviour {

	void OnCollisionEnter2D(Collision2D coll) {
		if (coll.gameObject.tag == "ObjectFalling") {
			Debug.Log ("Lost a ball/life");
			coll.gameObject.SetActive (false);
			PutInTheNest_GM.gmInstance.LostALife();
			PutInTheNest_GM.gmInstance.ballsInScene -= 1;
			if(PutInTheNest_GM.gmInstance.ballsInScene < 1){
				PutInTheNest_GM.gmInstance.RespawnObj();	
			}
			PutInTheNest_GM.gmInstance.RestartTimer();
		}
	}
}
