﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PutInTheNest_Hearts : MonoBehaviour {


	void OnTriggerEnter2D(Collider2D other) {
		if (other.gameObject.tag == "Character") {
			Debug.Log ("Player catch this life");
			PutInTheNest_GM.gmInstance.StartTimerHearts ();
			PutInTheNest_GM.gmInstance.WinALife ();
			gameObject.SetActive (false);
		}
		if (other.gameObject.tag == "Floor") {
			Debug.Log ("Player didnt catch this life");
			PutInTheNest_GM.gmInstance.StartTimerHearts ();
			gameObject.SetActive (false);
		}
	}
}
