﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PutInTheNest_GM : MonoBehaviour {

	public static PutInTheNest_GM gmInstance;
	public GameObject RespawnLeft, respawnRight;
	public GameObject ballSmall, ballMedium, ballBig;
	public ObjectPooler poolerHearts, currentPooler, poolerSmall, poolerMedium, poolerBig;
	public bool canRespawn = false;
	private float timeRespawn;
	public bool hasOneBallPlaying = true;
	public int ballsInScene = 0;
	public int points = 0;
	public int lifes = 3;


	void Awake(){
		if (gmInstance != null) {
			Destroy (this);
		} else {
			gmInstance = this;
		}
	}
	void Start(){
		StartGame ();
	}
	IEnumerator Timer(){
		Debug.Log ("Start Coroutine Timer");
		yield return new WaitForSeconds(timeRespawn);
		Debug.Log ("Timer will respawn");
		RespawnObj();
	}
	IEnumerator TimerHearts(){
		Debug.Log ("Start Coroutine Timer Hearts");
		float timeHearts = Random.Range (10.0f, 30.0f);
		Debug.Log ("time hearts: " + timeHearts);
		yield return new WaitForSeconds(timeHearts);
		Debug.Log ("Timer will respawn");
		RespawnHeart();
	}
	public void RestartTimer(){
		Debug.Log ("Restart Timer");
		StopCoroutine ("Timer");
		StartCoroutine ("Timer");
	}
	public void StartTimer(){
		Debug.Log ("Start Timer");
		StartCoroutine ("Timer");
	}
	public void StopTimer(){
		StopCoroutine ("Timer");
	}
	public void RespawnObj(){
		if (ballsInScene < 3 && canRespawn) {
			int randomPooler = Random.Range (1, 3);

			switch(randomPooler){
			case 1:
				currentPooler = poolerSmall;
				break;
			case 2: 
				currentPooler = poolerMedium;
				break;
			case 3: 
				currentPooler = poolerBig;
				break;
			default:
				Debug.LogWarning ("Error in pooler random");
				break;
			}

			int randomRespawn = Random.Range (0, 10);
			if (randomRespawn % 2 == 0) {
				ballsInScene += 1;
				GameObject obj = currentPooler.GetDesabledObject ();
				obj.transform.position = RespawnLeft.transform.position;
				obj.SetActive (true);
			} else {
				ballsInScene += 1;
				GameObject obj = currentPooler.GetDesabledObject ();
				obj.transform.position = respawnRight.transform.position;
				obj.SetActive (true);
			}
		} else {
			Debug.Log ("Já tem 3 bolas na cena");
		}

	}
	public void StartTimerHearts(){
		StartCoroutine ("TimerHearts");	
	}
	public void StopTimerHearts(){
		StopCoroutine ("TimerHearts");	
	}
	public void RespawnHeart(){
		if (canRespawn) {
			int randomRespawn = Random.Range (0, 10);
			if (randomRespawn % 2 == 0){
				GameObject obj = poolerHearts.GetDesabledObject ();
				obj.transform.position = RespawnLeft.transform.position;
				obj.SetActive (true);
			} else {
				GameObject obj = poolerHearts.GetDesabledObject ();
				obj.transform.position = respawnRight.transform.position;
				obj.SetActive (true);
			}
		}
	}
	public void GameOver(){
		canRespawn = false;
		StopTimer ();
		StopTimerHearts ();
	}
	public void RestartGame(){
		lifes = 3;
		points = 0;
		PutInTheNest_UI.uiInstance.UpdateUI ();
		hasOneBallPlaying = true;
		StartGame ();
	}
	public void StartGame(){
		timeRespawn = 15.0f;
		canRespawn = true;
		RespawnObj ();
		StartTimer ();
		StartTimerHearts ();
	}
	public void PutOntheNest(){
		points += 1;
		PutInTheNest_UI.uiInstance.UpdateUI ();
	}
	public void LostALife(){
		lifes -= 1;
		PutInTheNest_UI.uiInstance.UpdateUI ();
		if(lifes == 0){
			PutInTheNest_UI.uiInstance.GameOverUI ();
		}
	}
	public void WinALife(){
		lifes += 1;
		PutInTheNest_UI.uiInstance.UpdateUI ();
	}
}
