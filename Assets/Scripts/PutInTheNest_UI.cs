﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PutInTheNest_UI : MonoBehaviour {

	public static PutInTheNest_UI uiInstance;
	public GameObject canvasGame, canvasEndGame;
	public Text lifes, points, endGameText;


	// Use this for initialization
	void Awake(){
		if (uiInstance != null) {
			Destroy (uiInstance);
		} else {
			uiInstance = this;
		}
	}
	void Start(){
		canvasGame.SetActive (true);
		canvasEndGame.SetActive (false);
	}
	public void UpdateUI () {
		lifes.text = "Lives: " + PutInTheNest_GM.gmInstance.lifes.ToString ();
		points.text = PutInTheNest_GM.gmInstance.points.ToString ();
	}
	public void GameOverUI(){
		PutInTheNest_GM.gmInstance.GameOver();
		canvasGame.SetActive (false);
		canvasEndGame.SetActive (true);
		endGameText.text = "Você perdeu! Sua pontuação foi de " + PutInTheNest_GM.gmInstance.points + " pontos!";
	}
	public void RestartGame(){
		canvasEndGame.SetActive (false);
		canvasGame.SetActive (true);
		//call restart game GM
		PutInTheNest_GM.gmInstance.RestartGame();


	}
}
