﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PutInTheNest_Ball : MonoBehaviour {

	public bool isFallingDown = false;
	private float maxVelocityY = 8.0f;
	public float timeToRespawnAnotherBall = 5.0f;
	private bool canCallAgain = false;

	void Start(){
		canCallAgain = true;
	}
	void Update () {
		if (gameObject.GetComponent<Rigidbody2D> ().velocity.y < 0) {
			isFallingDown = true;
			//setar o collider do nest como true
		} else {
			isFallingDown = false;
			//setar collider do nest como false
		}
//		Debug.Log ("Velocity: " + gameObject.GetComponent<Rigidbody2D> ().velocity.y);
//		Debug.Log ("MAX VELOCITY: " + maxVelocityY);
		if (gameObject.GetComponent<Rigidbody2D> ().velocity.y > maxVelocityY) {
			gameObject.GetComponent<Rigidbody2D> ().velocity = Vector2.ClampMagnitude(gameObject.GetComponent<Rigidbody2D> ().velocity, maxVelocityY);
		}
	}
}
