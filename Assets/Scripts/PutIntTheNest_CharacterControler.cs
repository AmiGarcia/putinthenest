﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PutIntTheNest_CharacterControler : MonoBehaviour {

	public GameObject characterLeft, characterRight;
	public float speed, width;

	// Use this for initialization
	void Start () {
		speed = 0.3f;
		width = CameraScript.camWidth;
	}
	// Update is called once per frame
	void Update () {
		if(Input.GetMouseButton (0) && Input.mousePosition.x <= Screen.width / 2) {
			//if you are touching on the left side
			if (gameObject.transform.position.x - 1.23f <= -width) {
				Debug.Log ("Cannot walk");
			} else {
				gameObject.transform.Translate(Vector3.left * speed);
			}

		}
		if (Input.GetMouseButton (0) && Input.mousePosition.x > Screen.width / 2) {
			//if you are touching on the right side
			if (gameObject.transform.position.x + 1.23f >= width) {
				Debug.Log ("Cannot walk");
			} else {
				gameObject.transform.Translate(Vector3.right * speed);
			}
		}
		if(Input.GetKey(KeyCode.RightArrow)){
			if (gameObject.transform.position.x + 1.23f >= width) {
				Debug.Log ("Cannot walk");
			} else {
				gameObject.transform.Translate(Vector3.right * speed);
			}
		}
		if(Input.GetKey(KeyCode.LeftArrow)){
			if (gameObject.transform.position.x - 1.23f <= -width) {
				Debug.Log ("Cannot walk");
			} else {
				gameObject.transform.Translate(Vector3.left * speed);
			}

		}
	}
}
