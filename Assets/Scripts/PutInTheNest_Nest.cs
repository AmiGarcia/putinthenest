﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PutInTheNest_Nest : MonoBehaviour {

	void OnTriggerEnter2D(Collider2D other) {
		Debug.Log ("Trigger!!!");
		if (other.gameObject.tag == "ObjectFalling") {
			Debug.Log ("Bola passou");
			if(other.gameObject.GetComponent<PutInTheNest_Ball>().isFallingDown){
				Debug.Log ("GOTCHA!!!");
				//won a point!
				other.gameObject.SetActive(false);
				PutInTheNest_GM.gmInstance.PutOntheNest ();
				PutInTheNest_GM.gmInstance.ballsInScene -= 1;
				if(PutInTheNest_GM.gmInstance.ballsInScene < 1){
					PutInTheNest_GM.gmInstance.RespawnObj();	
				}
				//reset timer
				PutInTheNest_GM.gmInstance.RestartTimer();
			}

		}
	}
}
